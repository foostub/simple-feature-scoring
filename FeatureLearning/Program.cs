﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FeatureLearning
{
	class Program
	{
		static void Main(string[] args)
		{
			var customerProfile = new SimpleFeatureScoringModel();

			customerProfile.Like(new[] { "low-mileage", "recent-model", "one-owner", "truck", "v8" });
			customerProfile.NotLike(new[] { "sedan", "chevrolet" });

			customerProfile.Like(new[] { "ford", "dodge" });
			customerProfile.NotLike(new[] { "chevy" });

			var mostImportantFeatures = customerProfile
				.Features()
				.OrderByDescending(F => F.Weight)
				.ToArray();
		}
	}

	public class SimpleFeatureScoringModel
	{
		private Dictionary<string, double> _features = new Dictionary<string, double>();

		public IEnumerable<Feature> Features()
		{
			return _features.Select(F => new Feature { Name = F.Key, Weight = F.Value });
		}

		private void addFeatures(IEnumerable<string> features)
		{
			foreach (var feature in features)
			{
				if (_features.ContainsKey(feature) == false)
				{
					_features.Add(feature, 0);
				}
			}
		}

		public void Like(IEnumerable<string> features)
		{
			this.addFeatures(features.ToArray());

			foreach (var token in features)
			{
				this.Train(token, 1, 1);
			}
		}

		public void NotLike(IEnumerable<string> features)
		{
			this.addFeatures(features.ToArray());

			foreach (var token in features)
			{
				this.Train(token, 1, -1);
			}
		}

		public void Train(string feature, double input, double target)
		{
			var weight = _features[feature] * input;
			var output = Math.Tanh(weight);
			var error = target - output;
			var derivative = 1.0 - (output * output);
			var adjustment = error * derivative * input * .1;
			this._features[feature] += adjustment;
		}
	}

	public class Feature
	{
		public string Name { get; set; }
		public double Weight { get; set; }
	}
}
